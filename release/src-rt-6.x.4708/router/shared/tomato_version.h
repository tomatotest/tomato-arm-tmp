#ifndef __TOMATO_VERSION_H__
#define __TOMATO_VERSION_H__
#define TOMATO_MAJOR		"1"
#define TOMATO_MINOR		"28"
#define TOMATO_BUILD		"0007"
#define	TOMATO_BUILDTIME	"wto, 03 lis 2015 16:12:32 +0100"
#define TOMATO_VERSION		"1.28.0007 K26ARM USB VPN-64K"
#define TOMATO_SHORTVER		"1.28"
#endif
